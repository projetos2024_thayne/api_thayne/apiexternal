const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')
//LEMBRE DO IMPORT

router.get('/teacher/', teacherController.getTeacher),
router.post('/aluno/', alunoController.postAluno),
router.put('/editaraluno/', alunoController.editarAluno),
router.delete('/deletaraluno/:id', alunoController.deleteAluno),
router.get('/external/', JSONPlaceholderController.getUsers),
router.get('/external/io', JSONPlaceholderController.getUsersWedsiteIO)
router.get('/external/com', JSONPlaceholderController.getUsersWedsiteCOM)
router.get('/external/net', JSONPlaceholderController.getUsersWedsiteNET)
router.get('/external/filter', JSONPlaceholderController.getCountDomain);

module.exports = router